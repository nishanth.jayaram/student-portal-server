const express = require("express");
const router = express.Router();
const Student = require("../models/student");

// Fetch all students
router.get("/student", async (req, res) => {
  try {
    const students = await Student.find({});
    res.json({ data: students });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// Create a new student
router.post("/student", async (req, res) => {
  try {
    const newStudent = new Student(req.body);
    await newStudent.save();
    res.status(201).json({ data: newStudent });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// Update an existing student by ID
router.put("/student/:id", async (req, res) => {
  try {
    const updatedStudent = await Student.findByIdAndUpdate(
      req.params.id,
      req.body,
      { new: true }
    );

    if (!updatedStudent) {
      return res.status(404).json({ message: "Student not found" });
    }

    res.json({ data: updatedStudent });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// Delete a student by ID
router.delete("/student/:id", async (req, res) => {
  try {
    const deletedStudent = await Student.findByIdAndRemove(req.params.id);

    if (!deletedStudent) {
      return res.status(404).json({ message: "Student not found" });
    }

    res.json({ message: "Student deleted" });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

module.exports = router;
