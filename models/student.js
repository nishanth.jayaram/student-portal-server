const mongoose = require("mongoose");

const studentSchema = new mongoose.Schema({
  id: Number,
  first_name: String,
  last_name: String,
  age: String,
  gender: String,
  class: String,
  address: String,
});

module.exports = mongoose.model("Student", studentSchema);
